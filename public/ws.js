(function() {
	var WSChatApp = angular.module("WSChatApp", []);

	var WSChatCtrl = function($scope) {
		var wsChatCtrl = this;

		wsChatCtrl.chats = "";
		wsChatCtrl.message = "";

		var socket = new WebSocket("ws://localhost:3000/ws-chat");
		socket.onmessage = function(evt) {
			$scope.$apply(function() {
				wsChatCtrl.chats = evt.data + "\n" + wsChatCtrl.chats;
			});
		};

		wsChatCtrl.send = function() {
			socket.send(wsChatCtrl.message);
			wsChatCtrl.message = "";
		};
	};

	WSChatApp.controller("WSChatCtrl", [ "$scope", WSChatCtrl ]);
})();
