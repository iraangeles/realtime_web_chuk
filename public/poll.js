(function() {
	var AJAXPollApp = angular.module("AJAXPollApp", []);

	var pollTime = 1000;

	var AJAXPollCtrl = function($scope, $http) {
		var ajaxPollCtrl = this;

		ajaxPollCtrl.counter = 0;

		var pollServer = function() {
			$http.get("/ajax-counter")
				.then(function(result) {
					console.log(">> data = %s", result.data.value);
					ajaxPollCtrl.counter = result.data.value;

				}).finally(function() {
					setTimeout(pollServer, pollTime);
				});
		}


		ajaxPollCtrl.start = function() {
			setTimeout(pollServer, pollTime);
		}

		ajaxPollCtrl.stop = function() {
		}
	}

	AJAXPollApp.controller("AJAXPollCtrl", ["$scope", "$http", AJAXPollCtrl]);
})();
