(function() {

	var SSEChatApp = angular.module("SSEChatApp", []);

	SSEChatCtrl = function($scope, $http) {
		var sseChatCtrl = this;

		sseChatCtrl.messages = "";
		sseChatCtrl.text = "";

		var sse = new EventSource("/sse-chat?name=fred");
		sse.onopen = function() {
			$scope.$apply(function() {
				sseChatCtrl.messages = "Connected\n";
			});
		};

		sse.onmessage = function(evt) {
			$scope.$apply(function() {
				var message = evt.data + "\n";
				console.info("data = %s", message);
				sseChatCtrl.messages = message + sseChatCtrl.messages;
			});
		};

		sseChatCtrl.send = function() {
			$http.post("/sse-chat", { message: sseChatCtrl.text })
				.then(function() {
					sseChatCtrl.text = "";
				});
		}
	};

	SSEChatApp.controller("SSEChatCtrl", [ "$scope", "$http", SSEChatCtrl ]);

})();
