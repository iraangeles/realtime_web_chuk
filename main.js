//Load all the required libraries
var path = require("path");
var bodyParser = require("body-parser");
var express = require("express");
var sse = require("express-server-sent-events");

//Define a list of static resources
var staticDirs = [ "public", "demos", "bower_components" ];

//Create an instance of the app
var app = express();

var wsock = require("express-ws")(app);

var counter = 0;

//Install json middleware 
app.use(bodyParser.json());

//Add list of directories to route
for (var i in staticDirs)
	app.use(express.static(path.join(__dirname, staticDirs[i])));

var wsChatRoom = [];
//WebSocket
app.ws("/ws-chat", function(ws, req) {
	console.info("New WS connection");
	wsChatRoom.push(ws);

	ws.on("message", function(message) {
		process.nextTick(function() {
			for (var i in wsChatRoom)
				wsChatRoom[i].send(message);
		});
	});

	ws.on("close", function() {
		var idx = wsChatRoom.findIndex(function(s) { return (s == ws); });
		if (idx >= 0)
			wsChatRoom.splice(idx, 1);
	});
});

//Server Sent Events
app.get("/ajax-counter", function(req, res) {
	counter++;
	res.status(200)
		.json({value: counter});
});

var chatRoom = [];

app.get("/sse-chat", sse, function(req, res) {
	console.info("new connection from %s", req.query.name);
	res.sse.name = req.query.name
	process.nextTick(function() {
		chatRoom.push(res.sse);
	});
});

app.post("/sse-chat", function(req, res) {
	var message = "data: " + req.body.message + "\n\n";

	process.nextTick(function() {
		for (var i in chatRoom)
			chatRoom[i](message);
	});

	res.status(201).end();
});

/*
app.get("/sse-counter", sse, function(req, res) {
	//This request is SSE
	var myCounter = 0;

	//Run function every second
	setInterval(
		function() {
			myCounter++;
			res.sse("data: " + myCounter + "\n\n");
		},
		1000
	);
});
*/

//Define the port to listen to
app.set("port", process.env.APP_PORT || 3000);

//Start the application
app.listen(app.get("port"), function() {
	console.info("Application started at %s on port %d"
			, new Date(), app.get("port"));
});
